require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board
  attr_accessor :matches, :turn

  def initialize(guesser = HumanPlayer.new, referee = ComputerPlayer.new)
    @guesser = guesser
    @referee = referee
    @matches = []
    @turn = 0
  end

  def play
    referee.pick_secret_word
    until won? || lost?
      @turn += 1
      p display
      letter = guesser.get_guess
      if letter == referee.secret_word
        puts "incredible! #{guesser.name} guessed it"
        play_again
      end
      referee.check_guess(letter)
    end
    guesser.result(self)
  end

  def play_again
    puts "play again? (y/n)"
    input = gets.chomp
    if input == "y"
      Hangman.new.play
    else
      return
    end
  end


  def display
    referee.display.join(' ')
  end

  def won?
    referee.display.all?{|ch| ch != "_"}
  end

  def lost?
    turn == referee.secret_word.length * 2
  end

end

class HumanPlayer
  attr_reader :name

  def initialize(name = "collin")
    @name = name
  end

  def result(game)
    if game.won?
      puts "congrats! you beat the computer \n"
      p game.referee.secret_word
    else
      puts "you lost you wimp \n"
      puts "this was the word"
      p game.referee.secret_word
    end
    game.play_again
  end

  def get_guess
    puts "#{name}, please enter a letter to guess"
    gets.chomp
  end

end

class ComputerPlayer

  attr_reader :dict, :secret_word
  attr_accessor :matches

  def initialize
    @dict = dictionary
    @matches = []
  end

  def pick_secret_word
    # words = File.readlines(dict)
    # secret_word = words.sample.strip
    @secret_word = dict.sample.strip
    secret_word.length
  end

  def check_guess(letter)
    match = []
    secret_word.chars.each_with_index do |ch, idx|
      next unless letter == ch
      match << idx
      matches << idx
    end
    match
  end

  def word_hash
    word_hash = Hash.new
    secret_word.chars.each_with_index do |char, idx|
      word_hash[idx] = char
    end
    word_hash
  end

  def display
    display = []
    word_hash.each do |k, v|
     if matches.include?(k)
       display << v
     else
       display << "_"
     end
    end
    display
  end


  def guess(board)
  end

end

if $PROGRAM_NAME == __FILE__
  game = Hangman.new
  game.play
end
