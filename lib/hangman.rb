require 'byebug'

class Hangman
  MAX_GUESSES = 8
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @attempts_left = MAX_GUESSES
  end

  def setup
    length = referee.pick_secret_word
    guesser.register_secret_length(length)
    @board = ["_"] * length
  end

  def play
    setup
    while @attempts_left > 0
      p @board.join(' ')
      take_turn

      if won?
        p @board.join(' ')
        debugger
        puts "Guesser wins!"
        return
      end
    end
    puts "Guesser loses!"
    puts "the word was #{referee.require_secret}"
  end

  def won?
    @board.all?{|el| el != "_"}
  end

  def take_turn
    guess = guesser.guess(board)
    matches = referee.check_guess(guess)
    update_board(guess, matches)
    guesser.handle_response(guess, matches)
    if !matches.any?
      @attempts_left -= 1
    end
  end

  def update_board(guess, indexes)
    indexes.each {|idx| @board[idx] = guess}
  end

end

class HumanPlayer


  def pick_secret_word
    puts "write the length of the word you are thinking"
    input = gets.chomp
    until input.to_i.is_a?(Integer)
      puts "please type a valid number"
      input = gets.chomp
    end
    input.to_i
  end

  def check_guess(str)
    puts "the guess is #{str}"
    puts "what positions are #{str} in if any?"
    input = gets.chomp
    input.split(' ').map{|el| el.to_i}
  end

  def register_secret_length(length)
    puts "The words length is #{length}"
  end

  def guess(board)
    puts "please enter a letter to guess"
    guess = gets.chomp
    guess
  end

  def handle_response(guess, matches)
    if matches.any?
      puts "#{guess} was found at position #{matches}"
    else
      puts "#{guess} isn't in the word"
    end
  end

  def require_secret
    puts "What word were you thinking of?"
    gets.chomp
  end


end

class ComputerPlayer

  attr_reader :candidate_words

  def initialize(dictionary = File.readlines("dictionary.txt").map(&:chomp))
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(str)
    matches = []
    @secret_word.chars.each_with_index do |char, idx|
      matches << idx if str == char
    end
    matches
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select {|word| length == word.length}
  end

  def guess(board)

    freq_table = freq_table(board)

    most_frequent_letters = freq_table.sort_by { |letter, count| count }
    letter, _ = most_frequent_letters.last

    letter
  end

  def require_secret
    @secret_word
  end

  def handle_response(guess, matches)
    @candidate_words.reject! do |word|
      should_delete = false
        word.chars.each_with_index do |ch, idx|
          if (ch == guess) && (!matches.include?(idx))
            should_delete = true
            break
          elsif (ch != guess) && (matches.include?(idx))
            should_delete = true
            break
          end
        end
      should_delete
    end
  end

  private
    def freq_table(board)
      freq_table = Hash.new(0)
      @candidate_words.each do |word|
        board.each_with_index do |letter, index|
          freq_table[word[index]] += 1 if letter == "_"
        end
      end

      freq_table
    end

end


if __FILE__ == $PROGRAM_NAME
  Hangman.new({guesser: ComputerPlayer.new, referee: HumanPlayer.new}).play
end
